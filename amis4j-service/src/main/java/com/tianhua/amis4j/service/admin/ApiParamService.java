package com.tianhua.amis4j.service.admin;

import com.coderman.model.meta.bean.back.ParamBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebApiParamDO;
import com.tianhua.amis4j.infrast.mapper.WebApiParamMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * date: 2022/4/26
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class ApiParamService {

    @Resource
    private WebApiParamMapper webApiParamMapper;


    public void saveApiParam(ParamBean  paramBean){
        WebApiParamDO  webApiParamDO = new WebApiParamDO();
        BeanUtils.copyProperties(paramBean,webApiParamDO);
        webApiParamMapper.insert(webApiParamDO);
    }


    public PageBean selectPage(PageBean pageBean){
        List<WebApiParamDO> webApiParamDOList = webApiParamMapper.getPageList(pageBean);
        List<ParamBean> paramBeanList = new ArrayList<>();
        for (WebApiParamDO webApiParamDO : webApiParamDOList){
            ParamBean paramBean = new ParamBean();
            BeanUtils.copyProperties(webApiParamDO,paramBean);
            paramBean.buildParamField();
            paramBeanList.add(paramBean);
        }
        pageBean.setCount(webApiParamMapper.getPageCount(pageBean));
        pageBean.setRows(paramBeanList);
        return pageBean;
    }

    public void updateParamBean(ParamBean paramBean){
        WebApiParamDO webApiParamDO = new WebApiParamDO();
        BeanUtils.copyProperties(paramBean,webApiParamDO);
        webApiParamMapper.update(webApiParamDO);
    }

    public List<ParamBean> selectByProjectCode(String projectCode){
        List<WebApiParamDO> webApiParamDOList = webApiParamMapper.selectByProjectCode(projectCode);
        List<ParamBean> paramBeanList = new ArrayList<>();
        for (WebApiParamDO webApiParamDO : webApiParamDOList){
            ParamBean paramBean = new ParamBean();
            BeanUtils.copyProperties(webApiParamDO,paramBean);
            paramBean.buildParamField();
            paramBeanList.add(paramBean);
        }
        return paramBeanList;
    }


    public ParamBean getById(Long id){
        WebApiParamDO webApiParamDO = webApiParamMapper.getById(id);
        ParamBean paramBean = new ParamBean();
        BeanUtils.copyProperties(webApiParamDO,paramBean);
        paramBean.buildParamField();
        return paramBean;
    }

    public void delete(Long id){
        webApiParamMapper.deleteById(id);
    }

}
