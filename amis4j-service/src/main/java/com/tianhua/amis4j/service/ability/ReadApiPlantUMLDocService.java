package com.tianhua.amis4j.service.ability;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.back.ParamBean;
import com.coderman.model.meta.bean.back.WebCodeTemplateBean;
import com.google.common.collect.Lists;
import com.tianhua.amis4j.service.enums.ClassRelationEnum;
import com.coderman.model.meta.GlobalConstant;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Description:
 * date: 2022/4/8
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class ReadApiPlantUMLDocService {

    @Autowired
    private ReadPlantUMLDocService readPlantUMLDocService;

    //@Autowired
    //private RefreshService refreshService;

    private RefreshService refreshService = new RefreshService();

    /**
     * 适用于web上传方式
     * @param filePath
     * @return
     */
    public PlantUMLApiContextBean readDoc(String filePath){
        try {
            List<String> contentList = FileUtils.readLines(new File(filePath),"utf-8");
            PlantUMLApiContextBean plantUMLApiContextBean = exeParse(contentList);
            refreshService.refresh(plantUMLApiContextBean);
            return plantUMLApiContextBean;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 读取api文档模型
     * @param plantUMLFileName
     * @return
     */
    public PlantUMLApiContextBean readDoc(String webProjectAppName, String plantUMLFileName){

        List<String> contentList = readPlantUMLDocService.readDomainPlantDoc(webProjectAppName, plantUMLFileName);
        if(CollectionUtils.isEmpty(contentList)){
            return null;
        }
        PlantUMLApiContextBean plantUMLApiContextBean = exeParse(contentList);
        refreshService.refresh(plantUMLApiContextBean);
        return plantUMLApiContextBean;
    }

    private PlantUMLApiContextBean exeParse(List<String> contentList){
        List<String> elementList = new ArrayList<>();
        PlantUMLApiContextBean plantUmlContextBean = new PlantUMLApiContextBean();

        String currentPackage= "";
        //对class,enum,interface进行解析
        for(String str : contentList){
            String content = str.trim();
            if(StringUtils.isEmpty(content) || content.contains("@startuml") || content.startsWith(GlobalConstant.PLANT_DOC_IGNORE)){
                continue;
            }

            if(content.contains("package")){
                currentPackage = content.replace("package","").replace("{","").trim();
                continue;
            }

            if(content.contains("{")){
                elementList.add(content);
                continue;
            }
            if(content.contains("}")){
                elementList.add(content);
                parseClassElement(elementList,plantUmlContextBean,currentPackage);
                elementList.clear();
                continue;
            }
            elementList.add(content);
        }

        //对类与类的关系进行解析
        Map<String, List<String>> relationListMap = new HashMap<>();
        for(String str : contentList){
            if(StringUtils.isEmpty(str) || str.contains("@startuml")
                    || str.contains("package") || str.trim().contains("{") || str.trim().contains("}")){
                continue;
            }

            Map<String, String> relationMap = ClassRelationEnum.parseRelation(str);
            if(relationMap == null || relationMap.size()<2){
                continue;
            }
            List<String> relationList =  relationListMap.get(relationMap.get("class"));
            if(CollectionUtils.isEmpty(relationList)){
                relationList = new ArrayList<>();
            }
            relationList.add(relationMap.get("relation"));
            relationListMap.put(relationMap.get("class").trim(),relationList);
        }

        return plantUmlContextBean;
    }


    /**
     * 解析文件内容整体路由
     * @param elementList
     * @param plantUMLApiContextBean
     */
    private void parseClassElement(List<String> elementList, PlantUMLApiContextBean plantUMLApiContextBean, String currentPackage){
        String className = getClassName(elementList.get(0));
        if(className.toLowerCase().endsWith(GlobalConstant.API)){
            List<ApiBean> apiBeanList = buildApiBean(elementList);
            plantUMLApiContextBean.addBatchApiBean(apiBeanList);

            List<WebCodeTemplateBean> webArchTemplateBeanList = buildWebArchTemplateBeanList(elementList);

            webArchTemplateBeanList.stream().forEach(webArchTemplateBean -> {
                plantUMLApiContextBean.addTemplateConfig(apiBeanList.get(0).getModuleCode(), webArchTemplateBean);
            });
        }

        if(className.toLowerCase().endsWith(GlobalConstant.DTO) || className.toLowerCase().endsWith(GlobalConstant.VO)){
            ParamBean paramClassbean = buildClassBean(elementList);
            plantUMLApiContextBean.addParamClassBean(paramClassbean);
        }
        else if(className.toLowerCase().endsWith("enum")){
            /*EnumBean enumBean = builEnumBean(elementList);
            enumBean.buildPlantUMLPackage(currentPackage);
            plantUmlContextBean.addEnumBean(enumBean);*/
        }

    }
    /**
     * 解析class类型数据
     * @param elementList
     * @return
     */
    private ParamBean buildClassBean(List<String> elementList ){

        String[] array = elementList.get(0).trim().replace("{","").trim().split("\"");

        String classMetaInfoArr = array[1];
        List<FieldBean> fieldBeanList = getFieldBeanList(elementList.subList(1,elementList.size()));
        ParamBean classBean = new ParamBean();
        classBean.setFieldBeanList(fieldBeanList);

        classBean.setParamClassName(classMetaInfoArr.split("-")[1].replace("\"",""));
        classBean.setClassDesc(classMetaInfoArr.split("-")[0].replace("\"",""));

        return classBean;
    }


    private String getClassName(String ele){
        String [] classArr = ele.trim().replace("{","").split(" ");
        return  classArr[classArr.length - 1];
    }

    /**
     * 解析class类型数据
     * @param elementList
     * @return
     */
    private List<ApiBean> buildApiBean(List<String> elementList ){

        List<ApiBean> apiBeanList = new ArrayList<>();
        String moduleCode = "";
        String moduleName = "";
        for (String fieldStr : elementList.subList(1,elementList.size())){

            if(fieldStr.trim().contains("extend info")){
                break;
            }
            if(!fieldStr.trim().contains(":")){
                continue;
            }

            if(!fieldStr.contains("(") && StringUtils.isEmpty(moduleName)){
                String [] fieldArr = fieldStr.trim().split("-");
                String moduleInfo = fieldArr[1];
                moduleName = moduleInfo.split(":")[0].trim();
                moduleCode = moduleInfo.split(":")[1].trim();
                continue;
            }
            String[] fieldArr = fieldStr.trim().split(":");
            ApiBean apiBean = new ApiBean();
            apiBean.setApiDoc(fieldArr[0]);
            String [] methodArr = fieldArr[1].trim().split("\\.");
            apiBean.setMethodType(methodArr[1]);
            String [] methodReturnArr = methodArr[0].split("\\(");
            String returnParam = methodReturnArr[0].split(" ")[0];
            ParamBean returnParamBean = new ParamBean();
            returnParamBean.setParamClassName(returnParam);
            apiBean.setReturnParam(returnParamBean);

            apiBean.setApiUrl(methodReturnArr[0].split(" ")[1]);
            String param = methodReturnArr[1].replace(")","");

            apiBean.setParamBeanList(buildParamBean(param));
            apiBean.setModuleCode(moduleCode);
            apiBean.setModuleName(moduleName);
            apiBeanList.add(apiBean);
        }

        return apiBeanList;
    }

    /**
     * 解析class类型数据 获取扩展字段信息
     * @param elementList
     * @return
     */
    private List<WebCodeTemplateBean> buildWebArchTemplateBeanList(List<String> elementList ){

        List<WebCodeTemplateBean> webArchTemplateBeanList = new ArrayList<>();

        for (String fieldStr : elementList.subList(1,elementList.size())){
            if(fieldStr.toLowerCase().contains(GlobalConstant.TEMPLATE_JSON_KEY)){
                String fieldInfo = fieldStr.trim().split(":")[1];
                String templateName = fieldInfo.trim().replace("string","").replace("String","").trim();
                webArchTemplateBeanList.add(WebCodeTemplateBean.getInstance(templateName));
            }
        }

        return webArchTemplateBeanList;
    }


    /**
     * 构建参数模型
     * @param param
     * @return
     */
    private List<ParamBean> buildParamBean(String param){
        if(StringUtils.isEmpty(param.trim())){
            return Lists.newArrayList();
        }
        List<ParamBean> paramBeanList = new ArrayList<>();
        if(!param.contains(",")){
            if(param.contains(" ")){
                String [] paramArr = param.split(" ");
                ParamBean paramBean = ParamBean.getInstance(paramArr[1], paramArr[0]);
                paramBeanList.add(paramBean);
            }
            return paramBeanList;
        }

        String [] paramArr = param.split(",");
        for (String paramStr : paramArr){
            if(paramStr.contains(" ")){
                String [] paramStrArr = paramStr.split(" ");
                ParamBean paramBean = ParamBean.getInstance(paramStrArr[1], paramStrArr[0]);
                paramBeanList.add(paramBean);
            }
        }
        return paramBeanList;
    }


    /**
     * 获取类的属性列表
     * @param elementList
     * @return
     */
    private List<FieldBean> getFieldBeanList(List<String> elementList){
        List<FieldBean> fieldBeanList = new ArrayList<>();
        int extendIndex = 0;

        for (int i = 0 ;i< elementList.size();i ++){
            String fieldStr = elementList.get(i);
            if(isExtendLine(fieldStr)){
                extendIndex = i;
            }
            if(fieldStr.contains("(") || fieldStr.contains(")")){
                continue;
            }
            if(!fieldStr.trim().contains(":")){
                continue;
            }

            String[] fieldArr = fieldStr.trim().split(":");
            FieldBean fieldBean = new FieldBean();
            if(i > extendIndex && extendIndex > 0){
                fieldBean.setExtendFieldTag(true);
            }
            fieldBean.buildDesc(fieldArr[0]);
            if(fieldArr[1].trim().contains(" ")){
                String [] fields = fieldArr[1].trim().split(" ");
                fieldBean.setFieldName(fields[1]);
                fieldBean.setFieldType(fields[0]);
            }else {
                fieldBean.setFieldName(fieldArr[1]);
            }
            fieldBeanList.add(fieldBean);
        }
        return fieldBeanList;
    }

    /**
     * 判断是否是扩展标示
     * @param content
     * @return
     */
    private boolean isExtendLine(String content){
        return content.contains("..") && content.contains("extend") && content.contains("info");
    }


    public static void main(String[] args) {
        ReadApiPlantUMLDocService readApiPlantUMLDocService = new ReadApiPlantUMLDocService();
        String path = "/Users/dasouche/scworkspace/myspace/amis4j/amis4j-service/src/main/resources/api-model.puml";
        PlantUMLApiContextBean plantUMLApiContextBean = readApiPlantUMLDocService.readDoc(path);
        System.out.println(JSON.toJSONString(plantUMLApiContextBean));
    }
}
