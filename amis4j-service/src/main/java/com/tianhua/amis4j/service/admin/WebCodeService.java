package com.tianhua.amis4j.service.admin;

import com.coderman.model.meta.bean.back.WebCodeBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebDslConfigDO;
import com.tianhua.amis4j.infrast.mapper.WebDslConfigMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Description:
 * date: 2022/4/27
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class WebCodeService {

    @Resource
    private WebDslConfigMapper webDslConfigMapper;

    public void saveWebDslConfig(WebCodeBean webCodeBean){
        WebDslConfigDO webDslConfigDO = new WebDslConfigDO();
        BeanUtils.copyProperties(webCodeBean,webDslConfigDO);
        String version = buildVersion(webCodeBean);
        webDslConfigDO.setVersion(version);
        webDslConfigMapper.insert(webDslConfigDO);
    }

    public void updateWebDslConfig(WebCodeBean webCodeBean){
        WebDslConfigDO webDslConfigDO = new WebDslConfigDO();
        BeanUtils.copyProperties(webCodeBean,webDslConfigDO);
        webDslConfigMapper.update(webDslConfigDO);
    }

    public PageBean selectPage(PageBean pageBean){
        List<WebDslConfigDO> webDslConfigDOList = webDslConfigMapper.getPageList(pageBean);
        List<WebCodeBean> webDslConfigBeanList = new ArrayList<>();
        for (WebDslConfigDO webDslConfigDO : webDslConfigDOList){
            WebCodeBean webDslConfigBean = new WebCodeBean();
            BeanUtils.copyProperties(webDslConfigDO,webDslConfigBean);
            webDslConfigBeanList.add(webDslConfigBean);
        }
        pageBean.setCount(webDslConfigMapper.getPageCount(pageBean));
        pageBean.setRows(webDslConfigBeanList);
        return pageBean;
    }

    public List<WebCodeBean> selectByProjectCode(String projectCode){
        List<WebDslConfigDO> webDslConfigDOList = webDslConfigMapper.selectByProjectCode(projectCode);
        List<WebCodeBean> webDslConfigBeanList = new ArrayList<>();
        for (WebDslConfigDO webDslConfigDO : webDslConfigDOList){
            WebCodeBean webDslConfigBean = new WebCodeBean();
            BeanUtils.copyProperties(webDslConfigDO,webDslConfigBean);
            webDslConfigBeanList.add(webDslConfigBean);
        }

        return webDslConfigBeanList;
    }

    public WebCodeBean getById(Long id){
        WebDslConfigDO webDslConfigDO = webDslConfigMapper.getById(id);
        WebCodeBean webDslConfigBean = new WebCodeBean();
        BeanUtils.copyProperties(webDslConfigDO,webDslConfigBean);
        return webDslConfigBean;
    }

    /**
     * 版本构建
     * @param webCodeBean
     * @return
     */
    private String buildVersion(WebCodeBean webCodeBean){
        WebDslConfigDO webDslConfigDO = new WebDslConfigDO();
        BeanUtils.copyProperties(webCodeBean,webDslConfigDO);
        List<WebDslConfigDO> webDslConfigDOList = webDslConfigMapper.getAll(webDslConfigDO);
        if(CollectionUtils.isEmpty(webDslConfigDOList)){
            return "v1";
        }
        return  "v"+(webDslConfigDOList.size()+1);
    }

}
