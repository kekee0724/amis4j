package com.tianhua.amis4j.service.engine.factory.vue;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.container.ContainerBean;
import com.coderman.model.meta.bean.dsl.CodeFactoryService;
import com.coderman.model.meta.bean.dsl.amis.AmisContainerEnum;
import com.coderman.model.meta.service.FormElementFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Description:
 * date: 2022/4/29
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component(value = "vueTemplateDslJsonFactory")
public class VueCodeFactoryImpl implements CodeFactoryService {

    @Resource(name = "vueFormElementFactory")
    private FormElementFactory vueFormElementFactory;

    @Override
    public String buildWebCode(ContainerBean containerBean, PlantUMLApiContextBean plantUMLApiContextBean) {
        if(containerBean.getContainer() == null){
            return containerBean.buildRequestDslJson();
        }
        String containerName = containerBean.getContainer().getContainerName();
        if(AmisContainerEnum.isInsertForm(containerName)){
            return vueFormElementFactory.buildInsertForm(containerBean.getApiBean(), plantUMLApiContextBean);
        }

        if(AmisContainerEnum.isPageList(containerName)){
            return vueFormElementFactory.buildPageForm(containerBean.getApiBean(),plantUMLApiContextBean);
        }

        if(AmisContainerEnum.isUpdateForm(containerName)){
            return vueFormElementFactory.buildUpdateForm(containerBean.getApiBean(),plantUMLApiContextBean);
        }

        if(AmisContainerEnum.isDetailForm(containerName)){
            return vueFormElementFactory.buildDetailForm(containerBean.getApiBean(),plantUMLApiContextBean);
        }
        return containerBean.buildRequestDslJson();
    }

    @Override
    public String preBuildComponent(FieldBean fieldBean) {
        return vueFormElementFactory.preBuildComponent(fieldBean);
    }


}
