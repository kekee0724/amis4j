package com.tianhua.amis4j.service.app;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.engine.WebCodeGenerator;
import com.tianhua.amis4j.service.ability.ReadApiPlantUMLDocService;
import com.tianhua.amis4j.service.config.WebDslConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description:
 * date: 2022/4/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class WebDslServiceImpl implements WebDslService{

    @Autowired
    private ReadApiPlantUMLDocService readApiPlantUMLDocService;

    @Autowired
    private WebCodeGenerator webCodeGenerator;

    @Autowired
    private WebDslConfig webDslConfig;




    @Override
    public void generateWebDsl(String webProjectAppName, String apiDocFileName) {
        PlantUMLApiContextBean plantUMLApiContextBean = readApiPlantUMLDocService.readDoc(webProjectAppName,apiDocFileName);
        plantUMLApiContextBean.setWebArchName(webDslConfig.getDslapp());
        webCodeGenerator.createWebCode(plantUMLApiContextBean);
    }

}
