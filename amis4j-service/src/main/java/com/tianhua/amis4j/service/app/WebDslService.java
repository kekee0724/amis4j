package com.tianhua.amis4j.service.app;


/**
 * Description:
 * date: 2022/4/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface WebDslService {
    /**
     * 进行web dsl生成的入口
     */
    void generateWebDsl(String webProjectAppName,String apiDocFileName);
}
