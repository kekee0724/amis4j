package com.tianhua.amis4j.service.parser;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.TemplateParseContextBean;
import com.coderman.model.meta.bean.back.WebCodeTemplateBean;
import com.coderman.model.meta.bean.dsl.ICURDParse;
import com.coderman.model.meta.bean.dsl.ITemplateParse;
import com.tianhua.amis4j.service.admin.ProjectConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Description:
 * date: 2022/5/10
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Slf4j
@Service
public class TemplateParseImpl extends AbstractTemplateParse implements ITemplateParse {
    @Autowired
    private TemplateParseContainer templateParseContainer;

    @Autowired
    private ProjectConfigService projectConfigService;

    @Override
    public void parse(PlantUMLApiContextBean plantUMLApiContextBean) {

        Map<String, List<WebCodeTemplateBean>> archTemplateConfigMap = plantUMLApiContextBean.getTemplateConfigMap();
        rebuildArchTemplateConfigMap(plantUMLApiContextBean.getWebArchName(),archTemplateConfigMap);
        TemplateParseContextBean templateParseContextBean = new TemplateParseContextBean();
        templateParseContextBean.setPlantUMLApiContextBean(plantUMLApiContextBean);
        templateParseContextBean.setProjectSnapShotBean(projectConfigService.buildProjectSnapShotBean(plantUMLApiContextBean.getProjectCode()));

        for (Map.Entry<String,List<WebCodeTemplateBean>> entry : archTemplateConfigMap.entrySet()){
            for (WebCodeTemplateBean webArchTemplateBean : entry.getValue()){
                ICURDParse customParse = templateParseContainer.getParseService(plantUMLApiContextBean.getWebArchName(), webArchTemplateBean.getTemplateName());
                templateParseContextBean.setWebCodeTemplateBean(webArchTemplateBean);
                //执行自定义代码构建
                customParse.parse(templateParseContextBean);
            }
        }
    }


}
