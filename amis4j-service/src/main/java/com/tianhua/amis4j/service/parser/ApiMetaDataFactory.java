package com.tianhua.amis4j.service.parser;

import com.coderman.model.meta.bean.back.CreateCodeConfigBean;
import com.coderman.model.meta.bean.back.ProjectSnapShotBean;
import com.coderman.model.meta.enums.TemplateEleEnum;
import com.tianhua.amis4j.service.admin.ProjectConfigService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * date: 2022/5/18
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class ApiMetaDataFactory extends AbstractTemplateParse{

    /**
     * 自定义模版下依赖的API元数据信息
     * @param createCodeConfigBean
     * @return
     */
    public Map<String,Object> buildApiMetaDataMap(CreateCodeConfigBean createCodeConfigBean){
        String moduleCode = createCodeConfigBean.getModuleCode();
        ProjectSnapShotBean projectSnapShotBean = createCodeConfigBean.getProjectSnapShotBean();

        Map<String,Object> metaDataMap = new HashMap<>();
        metaDataMap.put(TemplateEleEnum.PROJECT_DEV_URL.getCode(),getProjectDevUrl(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.PROJECT_TEST_URL.getCode(),getProjectTestUrl(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.PROJECT_PRE_URL.getCode(),getProjectPreUrl(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.PROJECT_PRO_URL.getCode(),getProjectProUrl(moduleCode,projectSnapShotBean));


        metaDataMap.put(TemplateEleEnum.INSERT_API.getCode(),getInsertApi(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.DELETE_API.getCode(),getDeleteApi(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.UPDATE_API.getCode(),getUpdateApi(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.PAGE_API.getCode(),getPageApi(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.DETAIL_API.getCode(),getDetailApi(moduleCode,projectSnapShotBean));


        metaDataMap.put(TemplateEleEnum.MODULE_TITLE.getCode(),getModuleTitle(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.MODULE_CODE.getCode(),getModuleCode(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.MODULE_CONTEXT.getCode(),getModuleContext(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.MODULE_ROUTE_URL.getCode(),getModuleRouteUrl(moduleCode,projectSnapShotBean));


        metaDataMap.put(TemplateEleEnum.INSERT_API_PARAM.getCode(),getInsertApiParam(moduleCode,projectSnapShotBean,createCodeConfigBean.getWebAppName()));
        metaDataMap.put(TemplateEleEnum.DETAIL_API_PARAM.getCode(),getDetailApiParam(moduleCode,projectSnapShotBean,createCodeConfigBean.getWebAppName()));
        metaDataMap.put(TemplateEleEnum.UPDATE_API_PARAM.getCode(),getUpdateApiParam(moduleCode,projectSnapShotBean,createCodeConfigBean.getWebAppName()));
        metaDataMap.put(TemplateEleEnum.PAGE_API_PARAM.getCode(),getPageApiParam(moduleCode,projectSnapShotBean,createCodeConfigBean.getWebAppName()));

        metaDataMap.put(TemplateEleEnum.DETAIL_API_TITLE.getCode(),getDetailApiTitle(moduleCode,projectSnapShotBean));
        metaDataMap.put(TemplateEleEnum.UPDATE_API_TITLE.getCode(),getUpdateApiTitle(moduleCode,projectSnapShotBean));

        return metaDataMap;
    }
}
