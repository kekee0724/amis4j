package com.tianhua.amis4j.service.engine;

import com.coderman.model.meta.bean.dsl.CodeFactoryService;
import com.coderman.model.meta.service.FormElementFactory;
import com.tianhua.amis4j.service.engine.factory.amis.AmisCodeJsonFactoryImpl;
import com.tianhua.amis4j.service.engine.factory.vue.VueCodeFactoryImpl;
import com.tianhua.amis4j.service.enums.WebArchAppEnum;
import com.tianhua.amis4j.service.utils.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Description:
 * date: 2022/5/13
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
@Slf4j
public class CodeFactoryRoute {

    public CodeFactoryService getByWebArchName(String webArchName){
        if (WebArchAppEnum.AMIS.getAppCode().equals(webArchName)){
            return SpringContextHolder.getBean(AmisCodeJsonFactoryImpl.class);
        }
        else if(WebArchAppEnum.VUE_ELEMENT_UI.getAppCode().equals(webArchName)){
            return SpringContextHolder.getBean(VueCodeFactoryImpl.class);
        }
        log.error("不支持的web框架！");
        return null;
    }



}
