package com.tianhua.amis4j.service.ability;

import com.tianhua.amis4j.service.enums.WebArchAppEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Description:
 * date: 2022/4/18
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
@Slf4j
public class ReadTemplateWebJsonDocService {

    private static final String TEMPLATE = "template";
    @Autowired
    private ResourceLoader resourceLoader;

    /**
     * 读取json文件通用方法
     * @param webArchName
     * @param containerName
     * @return
     */
    public List<String> readDslJsonContent(String webArchName, String containerName){
        if(StringUtils.isEmpty(containerName)){
            log.info("containerName is empty,can't read content----------------!!!!!!!!!!");
            return null;
        }

        if(WebArchAppEnum.AMIS.getAppCode().equals(webArchName)){
            containerName = containerName + ".json";
        }

        if(WebArchAppEnum.VUE_ELEMENT_UI.getAppCode().equals(webArchName)){
            containerName = containerName + ".vue";
        }
        try {
            Resource resource = resourceLoader.getResource("classpath:"+TEMPLATE+"/"+webArchName+"/"+containerName);
            File file = resource.getFile();
            return FileUtils.readLines(file,"UTF-8");
        } catch (IOException e) {
            log.error("读取代码模版配置失败");
        }
        return null;
    }
}
