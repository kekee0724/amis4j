package com.tianhua.amis4j.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Description:
 * date: 2022/4/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Configuration
@Component
@PropertySource("classpath:webdslconfig.properties")
public class WebDslConfig {

    @Value("${tianhua.amis4j.appName}")
    private String webAppProjectName;

    @Value(("${tianhua.amis4j.apidocName}"))
    private String apiDocFileName;

    @Value("${tianhua.amis4j.dslapp}")
    private String dslapp;

    public String getWebAppProjectName() {
        return webAppProjectName;
    }

    public String getApiDocFileName() {
        return apiDocFileName;
    }

    public String getDslapp() {
        return dslapp;
    }

}
