package com.tianhua.amis4j.service.parser;

import com.coderman.model.meta.annotations.Template;
import com.coderman.model.meta.bean.back.*;
import com.coderman.model.meta.bean.dsl.ICURDParse;
import com.coderman.model.meta.enums.CodeFileTypeEnum;
import com.coderman.model.meta.enums.CodeLevelEnum;
import com.tianhua.amis4j.service.admin.WebCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Description:对modulecurd进行解析
 * date: 2022/5/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Slf4j
@Service
@Template(name = "modulecrud",webArchName = "amis")
public class AmisCurdParseImpl extends AbstractTemplateParse implements ICURDParse {

    @Autowired
    private WebCodeService webDslConfigService;

    @Override
    public void parse(TemplateParseContextBean templateParseContextBean) {

        String moduleTitle = getModuleTitle(templateParseContextBean.getModuleCode(),templateParseContextBean.getProjectSnapShotBean());
        String updateApi = getUpdateApi(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String pageApi = getPageApi(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String deleteApi = getDeleteApi(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getInsertForm = getInsertForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getDetailForm = getDetailForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getUpdateForm = getUpdateForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getPageForm = getPageForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());

        String templateContent = templateParseContextBean.getWebCodeTemplateBean().getTemplateContent();
        String newContent = templateContent.replace("$moduleTitle",moduleTitle)
                .replace("\"$insertForm\"",getInsertForm)
                .replace("$pageApi",pageApi)
                .replace("\"$detailForm\"",getDetailForm)
                .replace("$updateApi",updateApi)
                .replace("\"$updateForm\"",getUpdateForm)
                .replace("\"$pageForm\"",getPageForm)
                .replace("$deleteApi",deleteApi);


        log.info("解析后的content = "+newContent);

        WebCodeBean webDslConfigBean = new WebCodeBean();
        webDslConfigBean.setWebDslJson(newContent);
        webDslConfigBean.setModuleCode(templateParseContextBean.getModuleCode());
        webDslConfigBean.setCodeLevel(CodeLevelEnum.MODULE.getLevel());
        webDslConfigBean.setApiUrl("");
        webDslConfigBean.setSinglePageHtml("");
        webDslConfigBean.setContentType(CodeFileTypeEnum.JSON.getCodeType());
        webDslConfigBean.setProjectCode(templateParseContextBean.getProjectCode());
        webDslConfigBean.setVersion("V1.0");
        //持久化模块级的代码内容
        webDslConfigService.saveWebDslConfig(webDslConfigBean);

    }


}
