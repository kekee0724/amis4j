package com.tianhua.amis4j.web.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:
 * date: 2022/5/16
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class SiteVO {
    private List<SiteDetailVO> pages = new ArrayList<>();

    public void init(){
        SiteDetailVO siteDetailVO = new SiteDetailVO();
        siteDetailVO.setLabel("web前端代码管理系统");

        Map<String,String> siteProjectMap = new HashMap<>();
        siteProjectMap.put("label","项目应用管理");
        siteProjectMap.put("url","/project");
        siteDetailVO.addChild(siteProjectMap);


        Map<String,String> siteModuleMap = new HashMap<>();
        siteModuleMap.put("label","模块管理");
        siteModuleMap.put("url","/module");
        siteDetailVO.addChild(siteModuleMap);

        this.pages.add(siteDetailVO);
    }
}
