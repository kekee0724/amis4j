package com.tianhua.amis4j.web.convert;

import com.coderman.model.meta.bean.back.ProjectBean;
import com.tianhua.amis4j.web.vo.ProjectConfigVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Description:
 * date: 2022/4/19
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Mapper
public interface ProjectConfigConvert {
    ProjectConfigConvert INSTANCE = Mappers.getMapper(ProjectConfigConvert.class);

    ProjectBean convert2bean(ProjectConfigVO configVO);

    ProjectConfigVO convert2vo(ProjectBean projectBean);

    List<ProjectConfigVO> convert2beans(List<ProjectBean> projectBeanList);

}
