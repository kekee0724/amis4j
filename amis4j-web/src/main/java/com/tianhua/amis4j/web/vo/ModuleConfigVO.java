package com.tianhua.amis4j.web.vo;


import lombok.Data;

import java.util.List;

/**
* @Description:模块配置VO类
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@Data
public class ModuleConfigVO extends BaseVO{

	/**   主键 **/
	private Long id;
	/**  项目编码 **/
	private String projectCode;
	/**  模块编码 **/
	private String moduleCode;
	/**  模块描述 **/
	private String moduleDesc;
	/**  上下文名称 **/
	private String contextName;
	/**  模块路由url **/
	private String routeUrl;

	private List<WebApiVO> apiList;

	private String webAppName;


}