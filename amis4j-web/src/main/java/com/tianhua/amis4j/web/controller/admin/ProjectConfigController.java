package com.tianhua.amis4j.web.controller.admin;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.ProjectBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.service.admin.ProjectConfigService;
import com.tianhua.amis4j.service.admin.WebCodeDownloadService;
import com.tianhua.amis4j.web.convert.ProjectConfigConvert;

import com.tianhua.amis4j.web.utils.FileUtil;
import com.tianhua.amis4j.web.vo.query.ProjectConfigQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tianhua.amis4j.web.vo.ProjectConfigVO;


import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;


/**
* @Description:项目配置控制层
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@RestController
public class ProjectConfigController extends BaseController{
	
	protected Logger logger = LoggerFactory.getLogger(ProjectConfigController.class);

	@Autowired
	private ProjectConfigService projectConfigService;

	@Resource
	private HttpServletResponse response;

	@Resource
	private HttpServletRequest request;

	@Autowired
	private WebCodeDownloadService webCodeDownloadService;
	/**
     * @Description:新增项目配置
     * @version v1.0
     * @param projectConfigVO
     * @return ResultDto
     */
    @RequestMapping(value = "/projectConfig/add",method = RequestMethod.POST)
    public ResultDto add(@RequestBody ProjectConfigVO projectConfigVO){
		projectConfigVO.setWebDsl(projectConfigVO.getDslAppName());
		projectConfigVO.init();
		projectConfigVO.setGitAddress("asdfasdf");

		logger.info("projectConfigVO = "+ JSON.toJSONString(projectConfigVO));
		ProjectBean projectBean = ProjectConfigConvert.INSTANCE.convert2bean(projectConfigVO);
		ProjectBean oldBean = projectConfigService.getByProjectCode(projectBean.getProjectCode());
		if(oldBean != null){
			return ResultDto.fail("500","项目应用编码已存在!");
		}

		projectConfigService.saveProjectConfig(projectBean);

		projectConfigService.initProjectMetaData(projectBean,projectConfigVO.getFile());
		return ResultDto.success();
    }



	/**
	 * @Description:修改项目配置
	 * @version v1.0
	 * @param projectCode
	 * @return ResultDto
	 */
	@RequestMapping(value = "/projectConfig/getByCode/{projectCode}",method = RequestMethod.GET)
	public ResultDataDto getByCode(@PathVariable(value = "projectCode") String  projectCode){
		ProjectBean projectBean = projectConfigService.getByProjectCode(projectCode);
		ProjectConfigVO projectConfigVO = ProjectConfigConvert.INSTANCE.convert2vo(projectBean);
		return ResultDataDto.success(projectConfigVO);
	}




	/**
	 * @Description:修改项目配置
	 * @version v1.0
	 * @param projectConfigVO
	 * @return ResultDto
	 */
    @RequestMapping(value = "/projectConfig/update/{id}",method = RequestMethod.POST)
	public ResultDto update(@PathVariable(value = "id")Long id, @RequestBody  ProjectConfigVO projectConfigVO){
		projectConfigVO.setWebDsl("asdfasdf");
		projectConfigVO.init();
		projectConfigVO.setGitAddress("asdfasdf");

		ProjectBean projectBean = ProjectConfigConvert.INSTANCE.convert2bean(projectConfigVO);
		projectBean.setId(id);

		projectConfigService.updateProjectConfig(projectBean);
		return ResultDto.success();
	}

	/**
	 * @Description:分页获取项目配置记录
	 * @version v1.0
	 * @return ResultDataDto
	 */
	@GetMapping("/projectConfig/pagelist")
	public ResultDataDto getPage(ProjectConfigQuery pageVO){
		PageBean pageBean = pageVO.getPageBean();
		List<ProjectConfigVO> projectConfigVOList = ProjectConfigConvert.INSTANCE.convert2beans(projectConfigService.selectPage(pageBean).getRows());

		pageVO.setRows(projectConfigVOList);
		pageVO.setCount(pageBean.getCount());
		return ResultDataDto.success(pageVO);
	}

	@RequestMapping(value = "/projectConfig/search",method = RequestMethod.GET)
	public ResultDataDto select(String content){
		List<ProjectConfigVO> projectConfigVOList = ProjectConfigConvert.INSTANCE.convert2beans(projectConfigService.search(content));

		return ResultDataDto.success(wrapperProject(projectConfigVOList));
	}



	/**
	 * 下载项目代码
	 * @param projectCode
	 */
	@RequestMapping(value = "/projectConfig/downloadCode/{projectCode}",method = RequestMethod.GET)
	public void downloadModuleCode(@PathVariable(value = "projectCode")String projectCode) throws Exception {
		String webCodeZipFile = webCodeDownloadService.downloadProjectCode(projectCode);
		if(!webCodeZipFile.endsWith("zip")){
			throw new Exception(webCodeZipFile);
		}
		logger.info("zipCodePath =========== "+webCodeZipFile);
		File file = new File(webCodeZipFile);
		// 文件名称
		String fileName = "projectWebCode.zip";

		FileUtil.downloadFile(file, request, response, fileName);

	}

	@RequestMapping(value = "/projectConfig/refreshDoc",method = RequestMethod.POST)
	public ResultDto refreshDoc(@RequestBody ProjectConfigVO projectConfigVO){
		logger.info("projectConfigVO = "+JSON.toJSONString(projectConfigVO));
		projectConfigService.refreshApiDoc(projectConfigVO.getProjectCode(), projectConfigVO.getFile());
		return ResultDto.success();
	}

}
