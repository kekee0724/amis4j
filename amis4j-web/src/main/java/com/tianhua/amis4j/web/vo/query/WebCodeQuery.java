package com.tianhua.amis4j.web.vo.query;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.web.vo.PageVO;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * date: 2022/4/25
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class WebCodeQuery extends PageVO {
    private String  projectCode;

    private String moduleCode;

    private String apiUrl;

    private String contentType;

    private Integer codeLevel;

    private String webArchName;

    private String templateName;



    public PageBean getPageBean(){
        PageBean pageBean = super.getPageBean();
        Map<String,Object> query = new HashMap<>();
        if(StringUtils.isNotEmpty(this.moduleCode)){
            query.put("moduleCode",moduleCode);
        }
        if(StringUtils.isNotEmpty(this.projectCode)){
            query.put("projectCode",projectCode);
        }
        if(StringUtils.isNotEmpty(this.apiUrl)){
            query.put("apiUrl",apiUrl);
        }
        if(StringUtils.isNotEmpty(this.contentType)){
            query.put("contentType",contentType);
        }
        if(StringUtils.isNotEmpty(this.webArchName)){
            query.put("webArchName",webArchName);
        }

        if(StringUtils.isNotEmpty(this.templateName)){
            query.put("templateName",templateName);
        }
        if(codeLevel != null){
            query.put("codeLevel",codeLevel);
        }

        pageBean.setQuery(query);
        return pageBean;
    }
}
