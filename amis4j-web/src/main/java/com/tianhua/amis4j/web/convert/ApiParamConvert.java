package com.tianhua.amis4j.web.convert;

import com.coderman.model.meta.bean.back.ParamBean;
import com.tianhua.amis4j.web.vo.WebApiParamVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Description:
 * date: 2022/4/19
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Mapper
public interface ApiParamConvert {
    ApiParamConvert INSTANCE = Mappers.getMapper(ApiParamConvert.class);

    ParamBean convert2bean(WebApiParamVO apiVO);

    WebApiParamVO convert2vo(ParamBean paramBean);

    List<WebApiParamVO> convert2beans(List<ParamBean> apiBeanList);

}
