package com.tianhua.amis4j.web.controller.admin;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.WebCodeBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;
import com.tianhua.amis4j.service.admin.WebCodeService;
import com.tianhua.amis4j.web.convert.WebCodeConvert;
import com.tianhua.amis4j.web.vo.WebCodeVO;
import com.tianhua.amis4j.web.vo.query.WebCodeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
* @Description:webCode产物控制层
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@RestController
public class WebCodeController {
	
	protected Logger logger = LoggerFactory.getLogger(WebCodeController.class);

	@Autowired
	private WebCodeService webCodeService;

	/**
     * @Description:新增dsljson配置
     * @version v1.0
     * @param webCodeVO
     * @return ResultDto
     */
    @RequestMapping(value = "/webcode/add",method = RequestMethod.POST)
    public ResultDto add(@RequestBody WebCodeVO webCodeVO){
		webCodeService.saveWebDslConfig(WebCodeConvert.INSTANCE.convert2bean(webCodeVO));
		return ResultDto.success();
    }

	/**
	 * @Description:修改dsljson配置
	 * @version v1.0
	 * @param webCodeVO
	 * @return ResultDto
	 */
    @RequestMapping(value = "/webcode/update/{id}",method = RequestMethod.POST)
	public ResultDto update(@PathVariable(value = "id")Long id, @RequestBody WebCodeVO webCodeVO){
		WebCodeBean webCodeBean = WebCodeConvert.INSTANCE.convert2bean(webCodeVO);
		webCodeBean.setId(id);
		webCodeService.updateWebDslConfig(webCodeBean);
		return ResultDto.success();
	}

	/**
	 * @Description:根据id删除dsljson配置
	 * @version v1.0
	 * @param id
	 * @return ResultDto
	 */
	@PostMapping("/webcode/delete")
	public ResultDto delete(@RequestParam(name = "id") Long id){
		//todo impl code
		return new ResultDto();
	}

	/**
	 * @Description:分页获取模块dsljson记录
	 * @version v1.0
	 * @return ResultDataDto
	 */
	@GetMapping("/webcode/pagelist")
	public ResultDataDto getPage(WebCodeQuery query){
		logger.info("WebDslConfigQuery pageVo = "+ JSON.toJSONString(query));
		PageBean pageBean = query.getPageBean();
		List<WebCodeVO> webDslConfigVOS = WebCodeConvert.INSTANCE.convert2beans(webCodeService.selectPage(pageBean).getRows());
		query.setRows(webDslConfigVOS);
		query.setCount(pageBean.getCount());
		return ResultDataDto.success(query);
	}

	/**
	 * @Description:修改dsljson配置
	 * @version v1.0
	 * @param id
	 * @return ResultDto
	 */
	@RequestMapping(value = "/webcode/getById/{id}",method = RequestMethod.GET)
	public ResultDataDto getById(@PathVariable(value = "id")Long id){
		WebCodeBean webCodeBean = webCodeService.getById(id);
		WebCodeVO webCodeVO = WebCodeConvert.INSTANCE.convert2vo(webCodeBean);

		return ResultDataDto.success(webCodeVO);
	}


}
