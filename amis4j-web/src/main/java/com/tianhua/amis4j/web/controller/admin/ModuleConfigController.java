package com.tianhua.amis4j.web.controller.admin;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.ModuleBean;
import com.coderman.model.meta.bean.back.ProjectBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.coderman.model.meta.engine.WebCodeGenerator;
import com.tianhua.amis4j.service.admin.ModuleConfigService;
import com.tianhua.amis4j.service.admin.ProjectConfigService;
import com.tianhua.amis4j.web.convert.ApiConfigConvert;
import com.tianhua.amis4j.web.convert.ModuleConfigConvert;
import com.tianhua.amis4j.web.utils.FileUtil;
import com.tianhua.amis4j.web.vo.CreateCodeConfigVO;
import com.tianhua.amis4j.web.vo.query.ModuleConfigQuery;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tianhua.amis4j.web.vo.ModuleConfigVO;


import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Optional;


/**
* @Description:模块配置控制层
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@RestController
public class ModuleConfigController extends BaseController{
	
	protected Logger logger = LoggerFactory.getLogger(ModuleConfigController.class);

	@Autowired
	private ModuleConfigService moduleConfigService;

	@Autowired
	private ProjectConfigService projectConfigService;

	@Resource
	private HttpServletResponse response;

	@Resource
	private HttpServletRequest request;
	/**
     * @Description:新增模块配置
     * @version v1.0
     * @param moduleConfigVO
     * @return ResultDto
     */
    @RequestMapping(value = "/moduleConfig/add",method = RequestMethod.POST)
    public ResultDto add(@RequestBody ModuleConfigVO moduleConfigVO){
		moduleConfigVO.init();
		moduleConfigService.saveModuleConfig(ModuleConfigConvert.INSTANCE.convert2bean(moduleConfigVO));
		List<ModuleBean> moduleBeanList = moduleConfigService.getByProjectCode(moduleConfigVO.getProjectCode());
		if(moduleBeanList != null){
			Optional<ModuleBean> moduleBeanOptional = moduleBeanList.stream().filter(moduleBean -> moduleBean.getModuleCode().equals(moduleBean.getModuleCode())).findFirst();
			if(moduleBeanOptional.isPresent()){
				return ResultDto.fail("500","模块编码已存在!");
			}
		}

		return ResultDto.success();
    }


	/**
	 * @Description:基于模块维度和代码模版创建代码
	 * @version v1.0
	 * @param createCodeConfigVO
	 * @return ResultDto
	 */
	@RequestMapping(value = "/moduleConfig/createWebCode",method = RequestMethod.POST)
	public ResultDto createWebCode(@RequestBody CreateCodeConfigVO createCodeConfigVO){
		logger.info("createCodeConfigVO == "+JSON.toJSONString(createCodeConfigVO));

		moduleConfigService.createWebCode(ModuleConfigConvert.INSTANCE.convert2bean(createCodeConfigVO));
		return ResultDto.success();
	}



	/**
	 * @Description:修改模块配置
	 * @version v1.0
	 * @param moduleConfigVO
	 * @return ResultDto
	 */
    @RequestMapping(value = "/moduleConfig/update/{id}",method = RequestMethod.POST)
	public ResultDto update(@PathVariable(value = "id")Long id, @RequestBody ModuleConfigVO moduleConfigVO){
		moduleConfigVO.init();
		ModuleBean moduleBean = ModuleConfigConvert.INSTANCE.convert2bean(moduleConfigVO);
		moduleBean.setId(id);
		moduleConfigService.updateModuleConfig(moduleBean);
		return ResultDto.success();
	}

	/**
	 * @Description:分页获取模块配置记录
	 * @version v1.0
	 * @return ResultDataDto
	 */
	@GetMapping("/moduleConfig/pagelist")
	public ResultDataDto getPage(ModuleConfigQuery query){
		logger.info("ModuleConfigQuery pageVo = "+ JSON.toJSONString(query));
		PageBean pageBean = query.getPageBean();
		List<ModuleConfigVO> moduleConfigVOList = ModuleConfigConvert.INSTANCE.convert2beans(moduleConfigService.selectPage(pageBean).getRows());
		query.setRows(moduleConfigVOList);
		query.setCount(pageBean.getCount());
		return ResultDataDto.success(query);
	}

	@RequestMapping(value = "/moduleConfig/getById/{id}",method = RequestMethod.GET)
	public ResultDataDto getById(@PathVariable(name = "id") Long id){
		ModuleBean moduleBean = moduleConfigService.getById(id);
		ProjectBean projectBean = projectConfigService.getByProjectCode(moduleBean.getProjectCode());
		moduleBean.setWebAppName(projectBean.getWebDsl());

		ModuleConfigVO moduleConfigVO = ModuleConfigConvert.INSTANCE.convert2vo(moduleBean);
		moduleConfigVO.setApiList(ApiConfigConvert.INSTANCE.convert2beans(moduleBean.getApiBeanList()));
		return ResultDataDto.success(moduleConfigVO);
	}


	@RequestMapping(value = "/moduleConfig/search",method = RequestMethod.GET)
	public ResultDataDto select(String content){

		List<ModuleBean> moduleBeanList = moduleConfigService.search(content);
		List<ModuleConfigVO> moduleConfigVOList = ModuleConfigConvert.INSTANCE.convert2beans(moduleBeanList);
		return ResultDataDto.success(wrapperModule(moduleConfigVOList));
	}


	@RequestMapping(value = "/moduleConfig/getByProjectCode",method = RequestMethod.GET)
	public ResultDataDto getByProjectCode(String projectCode){
		List<ModuleBean> moduleBeanList = moduleConfigService.getByProjectCode(projectCode);
		List<ModuleConfigVO> moduleConfigVOList = ModuleConfigConvert.INSTANCE.convert2beans(moduleBeanList);
		return ResultDataDto.success(wrapperModule(moduleConfigVOList));
	}


	/**
	 * 下载模块代码
	 * @param id
	 */
	@RequestMapping(value = "/moduleConfig/downloadCode/{id}",method = RequestMethod.GET)
	public void downloadModuleCode(@PathVariable(value = "id")Long id) {
		String webCodeZipFile = moduleConfigService.downloadCode(id);

		logger.info("zipCodePath =========== "+webCodeZipFile);
		File file = new File(webCodeZipFile);
		// 文件名称
		String fileName = "moduleWebCode.zip";

		FileUtil.downloadFile(file, request, response, fileName);

	}

}
