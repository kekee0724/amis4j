package com.tianhua.amis4j.web.vo;


import lombok.Data;

/**
* @Description:项目配置VO类
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@Data
public class ProjectConfigVO extends BaseVO{

	/**   主键 **/
	private Long id;
	/**  项目应用名 **/
	private String projectCode;
	/**  应用描述 **/
	private String projectDesc;
	/**   业务领域编码 **/
	private String domainCode;
	/**   业务领域描述 **/
	private String domainDesc;
	/**  测试环境域名 **/
	private String testUrl;
	/**  测试环境域名 **/
	private String devUrl;
	/**  测试环境域名 **/
	private String preUrl;
	/**  测试环境域名 **/
	private String proUrl;
	/**  使用的web框架 **/
	private String webDsl;
	/**  项目git地址 **/
	private String gitAddress;

	/**
	 * 文件上传的文件名路径
	 */
	private String file;

	/**
	 * 使用的dsl框架名称
	 */
	private String dslAppName;

	/**
	 * 是否立即进行代码生成
	 */
	private boolean createCode;


}