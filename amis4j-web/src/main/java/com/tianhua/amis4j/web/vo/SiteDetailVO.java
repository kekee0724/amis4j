package com.tianhua.amis4j.web.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Description:
 * date: 2022/5/16
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class SiteDetailVO {
    private String label;

    private List<Map<String, String>> children = new ArrayList<>();

    public void addChild(Map<String, String> child){
        children.add(child);
    }

}
