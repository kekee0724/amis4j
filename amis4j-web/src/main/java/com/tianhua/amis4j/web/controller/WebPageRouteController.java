package com.tianhua.amis4j.web.controller;

import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;
import com.tianhua.amis4j.web.vo.SiteVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Description:
 * date: 2022/3/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Controller
public class WebPageRouteController {
    @RequestMapping("/")
    public ModelAndView home(){
        return new ModelAndView("home");
    }

    @RequestMapping("/table")
    public ModelAndView table(){
        return new ModelAndView("table");
    }


    @RequestMapping("/start")
    public ModelAndView start(){
        return new ModelAndView("start");
    }

    @RequestMapping("/project")
    public ModelAndView project(){
        return new ModelAndView("project");
    }

    @RequestMapping("/module")
    public ModelAndView module(){
        return new ModelAndView("module");
    }


    @RequestMapping("/import")
    public ModelAndView importPlantUML(){
        return new ModelAndView("import");
    }

    @RequestMapping("/webapi")
    public ModelAndView webapi(){
        return new ModelAndView("webapi");
    }

    @RequestMapping("/webapiparam")
    public ModelAndView webapiparam(){
        return new ModelAndView("webapiparam");
    }

    @RequestMapping("/webcode")
    public ModelAndView webdslconfig(){
        return new ModelAndView("webcode");
    }

    @RequestMapping("/webcodetemplate")
    public ModelAndView webarchtemplateconfig(){
        return new ModelAndView("webcodetemplate");
    }
}
