package com.tianhua.amis4j.web.controller.admin;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.back.ModuleBean;
import com.coderman.model.meta.bean.back.ParamBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebApiParamDO;
import com.tianhua.amis4j.service.admin.ApiParamService;
import com.tianhua.amis4j.web.convert.ApiConfigConvert;
import com.tianhua.amis4j.web.convert.ApiParamConvert;
import com.tianhua.amis4j.web.convert.ModuleConfigConvert;
import com.tianhua.amis4j.web.vo.WebApiVO;
import com.tianhua.amis4j.web.vo.query.WebApiParamQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tianhua.amis4j.web.vo.WebApiParamVO;


import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
* @Description:dto/vo参数表控制层
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@RestController
public class WebApiParamController{
	
	protected Logger logger = LoggerFactory.getLogger(WebApiParamController.class);


	@Autowired
	private ApiParamService apiParamService;
	/**
     * @Description:新增dto/vo参数表
     * @version v1.0
     * @param webApiParamVO
     * @return ResultDto
     */
    @RequestMapping(value = "/webApiParam/add",method = RequestMethod.POST)
    public ResultDto add(@RequestBody WebApiParamVO webApiParamVO){
		webApiParamVO.init();
		ParamBean paramBean = ApiParamConvert.INSTANCE.convert2bean(webApiParamVO);
		apiParamService.saveApiParam(paramBean);
		return ResultDto.success();
    }

	/**
	 * @Description:修改dto/vo参数表
	 * @version v1.0
	 * @param webApiParamVO
	 * @return ResultDto
	 */
    @RequestMapping(value = "/webApiParam/update/{id}",method = RequestMethod.POST)
	public ResultDto update(@PathVariable(value = "id")Long id, @RequestBody WebApiParamVO webApiParamVO){
		webApiParamVO.init();
		ParamBean paramBean = ApiParamConvert.INSTANCE.convert2bean(webApiParamVO);
		paramBean.setId(id);
		apiParamService.updateParamBean(paramBean);
		return ResultDto.success();
	}
	/**
	 * @Description:删除
	 * @version v1.0
	 * @return ResultDto
	 */
	@RequestMapping(value = "/webApiParam/delete/{id}",method = RequestMethod.DELETE)
	public ResultDto delete(@PathVariable(value = "id")Long id){
		apiParamService.delete(id);
		return ResultDto.success();
	}


	/**
	 * @Description:根据ID获取dto/vo参数表单条记录
	 * @version v1.0
	 * @param id
	 * @return ResultDataDto
	 */
	@GetMapping("/webApiParam/getById/{id}")
	public ResultDataDto getById(@PathVariable(name = "id") Long id){
		ParamBean paramBean = apiParamService.getById(id);
		WebApiParamVO webApiParamVO = ApiParamConvert.INSTANCE.convert2vo(paramBean);
		return ResultDataDto.success(webApiParamVO);
	}

	/**
	 * @Description:分页获取dto/vo参数表记录
	 * @version v1.0
	 * @return ResultDataDto
	 */
	@GetMapping("/webApiParam/pagelist")
	public ResultDataDto getPage(WebApiParamQuery query){
		logger.info("WebApiParamQuery pageVo = "+ JSON.toJSONString(query));
		PageBean pageBean = query.getPageBean();
		List<WebApiParamVO> apiVOList = ApiParamConvert.INSTANCE.convert2beans(apiParamService.selectPage(pageBean).getRows());
		apiVOList.forEach(projectConfigVO -> {
			projectConfigVO.setFieldBeanList(JSON.parseArray(projectConfigVO.getParamFieldJson(), FieldBean.class));
		});
		query.setRows(apiVOList);
		query.setCount(pageBean.getCount());
		return ResultDataDto.success(query);

	}

	/**
	 * @Description:修改dto/vo参数表状态
	 * @version v1.0
	 * @param id
     * @param status
	 * @return ResultDataDto
	 */
	@PostMapping("/webApiParam/changestatus")
	public ResultDto changeStatus(@RequestParam(name = "id") Long id,@RequestParam(name = "status") int status){
		//todo impl code
		return new ResultDto();
	}

}
