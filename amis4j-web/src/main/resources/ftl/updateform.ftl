{
"title": "${updateApiTitle}",
"body": [{
"type": "form",
"mode": "horizontal",
"api": "${updateApi}",
"actions": [{
"type": "submit",
"label": "提交",
"primary": true
}],
"collapsable": true,
"title":  "${updateApiTitle}",
"body": [

<#list updateApiParam as param>
    {
    "type": "${param.inputType}",
    "name": "${param.fieldName}",
    "label": "${param.fieldDesc}"
    <#if (param.required)??>
        ,"required": true
    </#if>
    },{
    "type": "divider"
    },
</#list>
    {
        "type": "divider"
    }
]
}]
}