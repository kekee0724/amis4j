{
"type": "page",
"title": "${moduleTitle}",
"remark": null,
"name": "api",
"toolbar": [
{
"type": "button",
"actionType": "link",
"link": "/${moduleCode}/new",
"label": "新增",
"primary": true
}
],
"body": [
{
"type": "crud",
"name": "sample",
"api": "${pageApi}",
"perPage": 5,
"keepItemSelectionOnPageChange": true,
"maxKeepItemSelectionLength": 11,
"labelTpl": "${r'${id} ${engine}'}",
"orderBy": "id",
"orderDir": "asc",
"autoGenerateFilter": true,
"filterTogglable": false,
"headerToolbar": [
"filter-toggler",
"bulkActions",
{
"type": "tpl",
"tpl": "定制内容示例：当前有 ${r'${count}'} 条数据。",
"className": "v-middle"
},
{
"type": "columns-toggler",
"align": "right"
},
{
"type": "drag-toggler",
"align": "right"
},
{
"type": "pagination",
"align": "right"
}
],
"footerToolbar": [
"statistics",
"switch-per-page",
"pagination"
],
"columns": [


<#list pageApiParam as param>
    {
    "type": "text",
    "name": "${param.fieldName}",
    "label": "${param.fieldDesc}"
    <#if (param.searchInPage)??>
        ,"searchable": {
            "type": "${param.inputType}",
            "name": "${param.fieldName}",
            "placeholder": "输入${param.fieldDesc}"
        }
    </#if>

    <#if (param.sortInPage)??>
        ,"sortable": true
    </#if>
    },
</#list>

{
"type": "operation",
"label": "操作",
"width": "",
"buttons": [
{
"type": "button-group",
"buttons": [
{
"type": "button",
"label": "查看",
"level": "primary",
"actionType": "link",
"link": "/${moduleCode}/${id}"
},
{
"type": "button",
"label": "修改",
"level": "info",
"actionType": "link",
"link": "/${moduleCode}/${id}/edit"
},
{
"type": "button",
"label": "删除",
"level": "danger",
"actionType": "ajax",
"confirmText": "您确认要删除?",
"api": "delete:${deleteApi}/$id"
}
]
}
],
"placeholder": "-",
"fixed": "right"
}
],
"affixHeader": true,
"columnsTogglable": "auto",
"placeholder": "暂无数据",
"tableClassName": "table-db table-striped",
"headerClassName": "crud-table-header",
"footerClassName": "crud-table-footer",
"toolbarClassName": "crud-table-toolbar",
"combineNum": 0,
"bodyClassName": "panel-default"
}
]
}
