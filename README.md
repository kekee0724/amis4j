# amis4jweb

### 介绍
天画产品线---前端低代码实现平台,基于amis低代码构建前端web页面，整体支持amis-json格式的
web代码生成,支持自定义代码文件模版,代码文件下载。在web框架方面除了支持amis，另外准备支持
AntDesign,LayUI,VUE-elementUI框架的web代码生成。

### 软件架构
#### 应用架构图
![image](doc/img/Amis4jweb-Web低代码应用架构图.png)
#### 工程模块说明
![image](doc/img/project-module.png)
#### plantUML API Doc说明
![image](doc/img/plantUMLDoc.png)

### 安装教程
1. 根据数据库脚本(doc/sql/amis4j-sql-v1.0.0.sql)初始化数据库表结构
2. clone项目,启动amis4j-web工程,默认端口8084
3. 前端项目进入amis4j-webfront文件夹,执行npm start或者node server.js
4. 前端页面通过http://localhost:3000访问
5. 上传的demo plantuml文件参考doc/demodoc/*.puml
### 界面展示(前后端分离)
#### 项目管理
![image](doc/img/index.png)
![image](doc/img/initProject.png)

#### web代码管理
![image](doc/img/modulecodecreate.png)
![image](doc/img/codelist.png)
![image](doc/img/code-amis-json.png)

#### 代码模版构建
![image](doc/img/templateVar.png)
![image](doc/img/addtemplate.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


