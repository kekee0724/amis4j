package com.coderman.model.meta.bean;

import lombok.Data;

/**
 * Description:
 * date: 2022/4/15
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class TemplateJsonBean {
    /**
     * $apiUrl
     * api链接
     */
    private String apiUrl;

    /**
     * $apiTitle
     * api标题
     * 或者表单标题什么的
     */
    private String apiTitle;

    /**
     * api对应的json信息
     */
    private String apiFieldList;

    /**
     * api所属模块名称
     */
    private String moduleCode;

    /**
     * 要使用的容器模版名称
     */
    private String containerTemplateName;

}
