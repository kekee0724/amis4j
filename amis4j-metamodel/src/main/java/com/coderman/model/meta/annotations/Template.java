package com.coderman.model.meta.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description:
 * date: 2022/5/10
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Template {
    /**
     * template模版名称
     * @return
     */
    String name() default "";

    /**
     * 适用的web框架
     * @return
     */
    String webArchName() default "amis";
}
