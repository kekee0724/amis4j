package com.coderman.model.meta.bean.dsl;


/**
 * Description:
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface ContainerDefine {
    /**
     * 获取容器名称
     */
    String getContainerName();

    /**
     * 获取容器描述
     */
    String getContainerDesc();

}
