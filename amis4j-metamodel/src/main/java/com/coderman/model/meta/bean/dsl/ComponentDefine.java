package com.coderman.model.meta.bean.dsl;

import java.util.Set;

/**
 * Description:
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface ComponentDefine {
    /**
     * 获取组件名
     */
    String getCompName();
    /**
     * 获取组件属性名
     */
    String getCompFieldName();

    /**
     * 获取组件描述
     */
    String getCompDesc();

    /**
     * 获取组件支持配置的其他属性列表
     * @return
     */
    Set<String> getSupportFieldSet();


}
