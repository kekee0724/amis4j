package com.coderman.model.meta.bean.back;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import lombok.Data;

/**
 * Description:
 * date: 2022/5/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class TemplateParseContextBean {

    private WebCodeTemplateBean webCodeTemplateBean;

    private ProjectSnapShotBean projectSnapShotBean;

    private PlantUMLApiContextBean plantUMLApiContextBean;


    public String getModuleCode(){
        if(webCodeTemplateBean == null){
            return "";
        }
        return webCodeTemplateBean.getModuleCode();
    }

    public String getProjectCode(){
        if(projectSnapShotBean == null){
            return "";
        }
        return projectSnapShotBean.getProjectCode();
    }
}
