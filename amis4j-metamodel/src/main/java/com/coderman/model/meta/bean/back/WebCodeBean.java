package com.coderman.model.meta.bean.back;

import lombok.Data;

/**
 * Description:
 * date: 2022/4/27
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class WebCodeBean {

    /**   主键 **/
    private Long id;

    /**  项目编码 **/
    private String projectCode;

    /**  模块编码 **/
    private String moduleCode;

    /**  api信息 **/
    private String apiUrl;

    /**  dsl配置内容 **/
    private String webDslJson;

    /**  单页面htmldemo **/
    private String singlePageHtml;

    /**  版本号 **/
    private String version;


    /**
     * 代码文件内容
     * JavaScript
     * JSON
     *
     */
    private String contentType;


    /**
     * 代码级别
     * API
     * MODULe
     */
    private String codeLevel;


    public boolean latestVersion(String version){
        Integer thisVersion = Integer.parseInt(this.version.replace("v",""));
        Integer oldVersion = Integer.parseInt(version.replace("v",""));
        if(thisVersion > oldVersion){
            return true;
        }
        return false;
    }
}
