package com.coderman.model.meta.enums;

/**
 * Description:
 * date: 2022/4/14
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum RequestSubmitEnum {
    POST("post"),
    GET("get"),
    PUT("put"),
    DELETE("delete"),
    ;
    String method;
    RequestSubmitEnum(String method){
        this.method = method;
    }

    public String getMethod() {
        return method;
    }
}
