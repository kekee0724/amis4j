package com.coderman.model.meta.bean.back;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.enums.RequestSubmitEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Description:
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ApiBean {
    /**
     * api访问链接
     */
    private String apiUrl;



    /**
     * post/get/delete
     */
    private String methodType;

    /**
     * api文档描述
     */
    private String apiDoc;

    /**
     * 响应json-案例
     */
    private String responseDataJson;

    /**
     * 所属模块
     */
    private String moduleCode;

    /**
     * 所属模块名称
     */
    private String moduleName;

    /**
     * 参数列表
     */
    private List<ParamBean> paramBeanList;


    /**
     * api对应的web端的dsl json配置
     */
    private String webDslJson;

    /**   主键 **/
    private Long id;

    /**  项目名称 **/
    private String projectCode;

    /**  请求json数据演示 **/
    private String requestJsonData;

    /**  响应json数据演示 **/
    private String responseJsonData;

    /**  请求参数元信息 **/
    private String requestParam;

    /**  创建时间 **/
    private Date dateCreate;

    /**  修改时间 **/
    private Date dateUpdate;

    /**  修改人 **/
    private Long updateUserId;

    /**  创建人 **/
    private Long createUserId;

    /**
     * api返回的对象信息
     */
    private ParamBean returnParam;


    /**
     * web代码模版名称---走持久化-动态模版配置
     */
    public String templateName;



    public boolean isPost(){
        return this.methodType.equals(RequestSubmitEnum.POST.getMethod());
    }

    public boolean isGet(){
        return this.methodType.equals(RequestSubmitEnum.GET.getMethod());
    }


    public boolean isPut(){
        return this.methodType.equals(RequestSubmitEnum.PUT.getMethod());
    }


    public boolean isDelete(){
        return this.methodType.equals(RequestSubmitEnum.DELETE.getMethod());
    }


    public void init(String projectCode){
        this.dateCreate = new Date();
        this.dateUpdate = new Date();
        this.updateUserId = 1L;
        this.createUserId = 1L;
        this.requestJsonData = "";
        this.responseJsonData = "";
        this.projectCode = projectCode;
    }

    public void buildParam(){
        if(StringUtils.isNotEmpty(this.requestParam)){
            this.paramBeanList = JSON.parseArray(this.requestParam,ParamBean.class);
        }
    }
}


