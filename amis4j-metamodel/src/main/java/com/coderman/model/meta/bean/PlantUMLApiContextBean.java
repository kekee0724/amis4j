package com.coderman.model.meta.bean;

import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.back.ParamBean;
import com.coderman.model.meta.bean.back.WebCodeTemplateBean;
import com.coderman.model.meta.bean.container.ContainerBean;
import com.coderman.utils.kvpair.KVPair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class PlantUMLApiContextBean {

    /**
     * 前端应用框架名称
     */
    private String webArchName;

    /**
     * 目标项目的项目编码
     */
    private String projectCode;


    /**
     * api模型列表
     */
    private List<ApiBean> apiBeanList = new ArrayList<>();


    private List<ParamBean>  paramClassBeanList = new ArrayList<>();



    /**
     * api参数自己指定类型的前端容器和组件
     *
     */
    private Map<String,List<KVPair>> apiParamFieldAttrMap = new HashMap<>();


    /**
     * key:模块编码
     * value:该模块下的所有容器和相关的api
     */
    private Map<String, List<ContainerBean>> moduleDslContainerMap = new HashMap<>();

    /**
     * key:模块编码
     * value:该模块下的所有个性化的低代码模版配置信息
     */
    private Map<String, List<WebCodeTemplateBean>> templateConfigMap = new HashMap<>();

    /**
     * key:模块编码
     * value:构建完成的json dsl 信息
     */
    private Map<String, List<TemplateJsonBean>> moduleDslJsonMap = new HashMap<>();

    public Map<String, List<TemplateJsonBean>> getModuleDslJsonMap() {
        return moduleDslJsonMap;
    }


    public List<ApiBean> getApiBeanList() {
        return apiBeanList;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Map<String, List<KVPair>> getApiParamFieldAttrMap() {
        return apiParamFieldAttrMap;
    }

    public Map<String, List<ContainerBean>> getModuleDslContainerMap() {
        return moduleDslContainerMap;
    }

    /**
     * 添加API-Bean
     * @param apiBean
     */
    public void addApiBean(ApiBean apiBean){
        apiBeanList.add(apiBean);
    }

    /**
     * 添加API-Bean
     * @param apiBeanList
     */
    public void addBatchApiBean(List<ApiBean> apiBeanList){
        if(apiBeanList == null || apiBeanList.isEmpty()){
            return;
        }

        for (ApiBean apiBean : apiBeanList){
            addApiBean(apiBean);
        }
    }

    public void addParamClassBean(ParamBean paramBean){
        this.paramClassBeanList.add(paramBean);
    }


    public String getWebArchName() {
        return webArchName;
    }

    public void setWebArchName(String webArchName) {
        this.webArchName = webArchName;
    }

    public Map<String, List<WebCodeTemplateBean>> getTemplateConfigMap() {
        return templateConfigMap;
    }

    /**
     * 根据参数类型名称获取属性列表
     * @param paramClassName
     * @return
     */
    public List<FieldBean> getParamClassFieldBeanList(String paramClassName){
        if(this.paramClassBeanList == null || this.paramClassBeanList.isEmpty()){
            return null;
        }
        for (ParamBean paramBean : paramClassBeanList){
            if(paramBean.getParamClassName().equals(paramClassName)){
                return paramBean.getFieldBeanList();
            }
        }
        return null;
    }

    /**
     * 增加container
     * @param module
     * @param containerBean
     */
    public void addContainer(String module,ContainerBean containerBean){
        if(module == null || module.equals("")){
            return;
        }

        List<ContainerBean> containerBeanList = this.moduleDslContainerMap.get(module);
        if(containerBeanList == null){
            containerBeanList = new ArrayList<>();
        }
        containerBeanList.add(containerBean);
        this.moduleDslContainerMap.put(module,containerBeanList);
    }

    /***
     * 增加模版配置
     * @param moduleName
     * @param webArchTemplateBean
     */
    public void addTemplateConfig(String moduleName, WebCodeTemplateBean webArchTemplateBean){
        List<WebCodeTemplateBean> webArchTemplateBeanList = templateConfigMap.get(moduleName);
        if(webArchTemplateBeanList == null){
            webArchTemplateBeanList = new ArrayList<>();
        }
        webArchTemplateBeanList.add(webArchTemplateBean);
        templateConfigMap.put(moduleName,webArchTemplateBeanList);
    }

    /**
     * 增加dsljson 信息
     * @param moduleName
     * @param templateJsonBean
     */
    public void addDslJson(String moduleName,TemplateJsonBean templateJsonBean){
        if(moduleName == null || moduleName.equals("")){
            return;
        }

        List<TemplateJsonBean> templateJsonBeanList = this.moduleDslJsonMap.get(moduleName);
        if(templateJsonBeanList == null){
            templateJsonBeanList = new ArrayList<>();
        }
        templateJsonBeanList.add(templateJsonBean);
        this.moduleDslJsonMap.put(moduleName,templateJsonBeanList);
    }

    public List<ParamBean> getParamClassBeanList() {
        return paramClassBeanList;
    }

    public void setParamClassBeanList(List<ParamBean> paramClassBeanList) {
        this.paramClassBeanList = paramClassBeanList;
    }




}
