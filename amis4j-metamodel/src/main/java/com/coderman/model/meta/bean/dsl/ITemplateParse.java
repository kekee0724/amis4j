package com.coderman.model.meta.bean.dsl;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;

/**
 * Description:自定义模块级别的curd整体配置内容
 * 或者自定义低代码模版配置实现
 * date: 2022/5/10
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface ITemplateParse {
    /**
     * 根据代码模版信息与配置信息进行结合生成目标源代码片段或者文件
     * @param plantUMLApiContextBean
     */
    void parse(PlantUMLApiContextBean plantUMLApiContextBean);
}
