package com.coderman.model.meta.bean.back;


import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.GlobalConstant;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Description:api参数模型
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ParamBean {

    /**
     * 参数名
     */
    private String paramName;

    /**
     * 参数类名称
     */
    private String paramClassName;

    /**
     * 参数类描述
     */
    private String classDesc;

    /**
     * 参数包名
     */
    private String packageName;

    private String projectCode;

    /**
     * 属性列表
     */
    List<FieldBean> fieldBeanList;

    /**
     *
     * 扩展属性列表
     * key:内置属性如xxxKeyList
     * 或者组件类型，如input-email,select
     * value为对应的属性名
     */
    Map<String,List<String>> fieldExtendBeanMapList;


    /**   主键 **/
    private Long id;


    /**  项目应用名 **/
    private String moduleCode;


    /**  参数类描述 **/
    private String paramClassDesc;

    /**  参数属性集合json **/
    private String paramFieldJson;

    /**  创建时间 **/
    private Date dateCreate;

    /**  修改时间 **/
    private Date dateUpdate;

    /**  修改人 **/
    private Long updateUserId;

    /**  创建人 **/
    private Long createUserId;

    public ParamBean(){}

    public ParamBean(String paramName, String paramClassName){
        this.paramName = paramName;
        this.paramClassName = paramClassName;
    }



    public static  ParamBean getInstance(String paramName, String paramClassName){
        return new ParamBean(paramName, paramClassName);
    }

    /**
     * 判断是否是表单数据容器模型
     * dto,vo,entity,model
     * @return
     */

    public boolean isModelParam(){
        if(StringUtils.isEmpty(this.paramClassName)){
            return false;
        }
        return this.paramClassName.toLowerCase().endsWith(GlobalConstant.DTO) || this.paramClassName.toLowerCase().endsWith(GlobalConstant.VO);
    }

    public void init(String projectCode){
        this.projectCode = projectCode;
        this.dateCreate = new Date();
        this.dateUpdate = new Date();
        this.createUserId = 1L;
        this.updateUserId = 1L;
        this.paramFieldJson = JSON.toJSONString(this.fieldBeanList);
        if(StringUtils.isEmpty(this.paramClassDesc)){
            this.paramClassDesc = "";
        }
        if(StringUtils.isEmpty(this.moduleCode)){
            this.moduleCode = "";
        }
    }

    public void buildParamField(){
        if(StringUtils.isNotEmpty(this.paramFieldJson)){
            this.fieldBeanList = JSON.parseArray(this.paramFieldJson,FieldBean.class);
        }
    }


}
