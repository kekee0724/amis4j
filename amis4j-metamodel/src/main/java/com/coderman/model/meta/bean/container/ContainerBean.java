package com.coderman.model.meta.bean.container;

import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.dsl.ContainerDefine;
import com.coderman.utils.kvpair.KVPair;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ContainerBean {
    /**
     * 容器属性列表
     */
    private List<KVPair> attributeList;
    /**
     * 组件列表--- 表单元素，或者列表中的每列元素数据等
     *
     */
    private List<ComponentBean> requestParamCompList;


    /**
     * 组件列表--- 表单元素，或者列表中的每列元素数据等
     *
     */
    private List<ComponentBean> responseParamCompList;

    /**
     * 子容器
     */
    private ContainerBean subContainer;

    /**
     * 容器信息
     */
    private ContainerDefine container;

    /**
     * 容器对应的api模型
     */
    private ApiBean apiBean;



    /**
     * 增加组件元素
     * @param componentBean
     */
    public void addRequestComponent(ComponentBean componentBean){
        if(this.requestParamCompList == null){
            this.setRequestParamCompList(new ArrayList<>());
        }
        this.getRequestParamCompList().add(componentBean);
    }

    /**
     * 增加组件元素
     * @param componentBean
     */
    public void addResponseComponent(ComponentBean componentBean){
        if(this.responseParamCompList == null){
            this.setResponseParamCompList(new ArrayList<>());
        }
        this.getResponseParamCompList().add(componentBean);
    }

    /**
     * 构建容器级别的web dsl json
     * @return
     */
    public String buildRequestDslJson(){
        if(this.requestParamCompList == null || this.requestParamCompList.isEmpty()){
            return "[]";
        }
        StringBuilder dslJsonBuilder = new StringBuilder("[");
        for (ComponentBean componentBean : requestParamCompList){
            dslJsonBuilder.append("{" + componentBean.buildJsonStr()+"},");
        }
        return dslJsonBuilder.substring(0,dslJsonBuilder.length() - 1)+"]";
    }
}
