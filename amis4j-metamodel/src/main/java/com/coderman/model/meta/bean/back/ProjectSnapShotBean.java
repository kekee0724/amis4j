package com.coderman.model.meta.bean.back;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Description:项目聚合模型
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ProjectSnapShotBean {

    /**
     * 项目编码
     */
    private String projectCode;

    /**
     * 项目配置信息
     */
    private ProjectBean projectBean;

    /**
     * 模块配置信息
     */
    private Map<String,ModuleBean> moduleBeanMap;

    /**
     * key:moduleCode
     * value:apiBeanList
     * api配置信息
     */
    private Map<String,List<ApiBean>> apiBeanListMap;

    /**
     * api参数模型配置信息
     */
    private Map<String,ParamBean> paramBeanMap;


    /**
     * 当前项目对应的webdsl配置信息
     */
    private List<WebCodeBean> webDslConfigBeanList;

}
