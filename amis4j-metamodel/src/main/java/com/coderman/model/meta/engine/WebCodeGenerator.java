package com.coderman.model.meta.engine;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.CreateCodeConfigBean;

/**
 * Description:
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface WebCodeGenerator {
    /**
     * 进行web dsl生成的入口--- 适用与全量构建
     * @param plantUMLApiContextBean
     */
    void createWebCode(PlantUMLApiContextBean plantUMLApiContextBean);

    /**
     * 根据模块维度+自定义模版生成代码---适用与增量构建
     * @param createCodeConfigBean
     */
    void createWebCode(CreateCodeConfigBean createCodeConfigBean);
}
