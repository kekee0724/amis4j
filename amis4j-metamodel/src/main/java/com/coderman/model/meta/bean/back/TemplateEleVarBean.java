package com.coderman.model.meta.bean.back;

import lombok.Data;

/**
 * Description: API 元数据提炼的模版变量模型
 * date: 2022/5/24
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class TemplateEleVarBean {
    private String eleCode;
    private String eleDesc;

    /**
     * json格式的引用方式
     */
    private String varJson;

    /**
     * freemarker格式的引用方式
     */
    private String varFreemarker;


    private String demo;


    public TemplateEleVarBean(String eleCode, String eleDesc){
        this.eleCode = eleCode;
        this.eleDesc = eleDesc;
    }

    public static  TemplateEleVarBean TemplateEleVarBean(String eleCode, String eleDesc){
        return new TemplateEleVarBean(eleCode, eleDesc);
    }


}
