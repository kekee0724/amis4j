package com.coderman.model.meta.bean.dsl;

import com.coderman.model.meta.bean.back.TemplateParseContextBean;

/**
 * Description:自定义模块级别的curd整体配置内容
 * 或者自定义低代码模版配置实现
 * date: 2022/5/10
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface ICURDParse {
    /**
     * 自定义模版代码解析与生成接口
     * @param templateParseContextBean
     */
    void parse(TemplateParseContextBean templateParseContextBean);
}
