package com.coderman.model.meta.enums;

/**
 * Description:
 * date: 2022/5/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum CodeFileTypeEnum {
    VUE("vue",".vue","vue格式"),
    JSON("json",".json","json格式"),
    JAVASCRIPT("js",".js","javascript脚本内容"),
    HTML("html",".html","html格式的内容"),
    FREEMARKER("ftl",".ftl","ftl格式的内容"),


    ;

    public String getCodeType() {
        return codeType;
    }

    public String getCodeFileSubfix() {
        return codeFileSubfix;
    }

    public String getDesc() {
        return desc;
    }

    private String codeType;

    private String codeFileSubfix;

    private String desc;

    CodeFileTypeEnum(String codeType, String codeFileSubfix, String desc){
        this.codeType = codeType;
        this.codeFileSubfix = codeFileSubfix;
        this.desc = desc;
    }



}
