package com.tianhua.amis4j.infrast.dataobject;

import lombok.Data;
import lombok.ToString;

 /**
 * @Description:项目配置DO类
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Data
@ToString
public class WebDslConfigDO{


	/**   主键 **/
	private Long id;

	/**  项目编码 **/
	private String projectCode;

	/**  模块编码 **/
	private String moduleCode;

	/**  api信息 **/
	private String apiUrl;

	/**  代码内容 **/
	private String webDslJson;

	/**  单页面代码内容html形式 **/
	private String singlePageHtml;

	/**  版本号 **/
	private String version;

	 /**
	  * 代码文件内容
	  * JavaScript
	  * JSON
	  *
	  */
	private String contentType;


	 /**
	  * 代码级别
	  * API
	  * MODULe
	  */
	private String codeLevel;

}