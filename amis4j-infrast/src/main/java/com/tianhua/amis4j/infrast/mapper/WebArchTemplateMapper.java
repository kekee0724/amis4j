package com.tianhua.amis4j.infrast.mapper;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebArchTemplateDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @Description:项目配置mapperDAO接口
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Mapper
public interface WebArchTemplateMapper {

	/**
	 * 
	 * @Description:新增或修改
	 * @param webArchTemplateDO
	 * @return int
	 */
	public long insert(WebArchTemplateDO webArchTemplateDO);

	/**
	 * @Description: 通过id删除数据
	 * @param id
	 * @return int
	 */
	public int deleteById(Long id);

	/**
	 * @Description: 通过id查询
	 * @param id
	 */
	public WebArchTemplateDO getById(Long id);

	/**
	 * 通过名称查询配置信息
	 * @param archName
	 * @param templateName
	 * @return
	 */
	public WebArchTemplateDO getByTemplateNameAndArchName(String archName,String templateName);


	/**
	 * @Description:查询所有数据
	 * @return List<WebArchTemplateDO>
	 */
	public List<WebArchTemplateDO>  getAll();

	/**
	*
	* @Description:新增或修改
	* @param webArchTemplateDO
	* @return int
	*/
	public int update(WebArchTemplateDO webArchTemplateDO);

	/**
	 * @Description:查询所有数据
	 * @return List<WebArchTemplateDO>
	 */
	 public List<WebArchTemplateDO>  getPageList(@Param(value = "page") PageBean page);


	/**
	 * @Description:查询数量
	 * @return int
	 */
	 public int  getPageCount(@Param(value = "page") PageBean pageBean);


}