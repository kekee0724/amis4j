package com.tianhua.amis4j.infrast.mapper;

import java.util.List;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebApiParamDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @Description:dto/vo参数表mapperDAO接口
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Mapper
public interface WebApiParamMapper{

	/**
	 * 
	 * @Description:新增或修改
	 * @param webApiParamDO
	 * @return int
	 */
	public long insert(WebApiParamDO webApiParamDO);

	/**
	 * @Description: 通过id删除数据
	 * @param id
	 * @return int
	 */
	public int deleteById(Long id);

	/**
	 * @Description: 通过id查询
	 * @param id
	 * @return ResultDataDto<WebApiParamDO>
	 */
	public WebApiParamDO getById(Long id);

	/**
	 * @Description:查询所有数据
	 * @return List<WebApiParamDO
	 */
	public List<WebApiParamDO>  getAll();

	/**
	 * @Description:查询所有数据
	 * @return List<WebApiParamDO
	 */
	public List<WebApiParamDO>  selectByProjectCode(String projectCode);
	/**
	*
	* @Description:新增或修改
	* @param webApiParamDO
	* @return int
	*/
	public int update(WebApiParamDO webApiParamDO);

	/**
	 * @Description:查询所有数据
	 * @return List<WebApiParamDO
	 */
	 public List<WebApiParamDO>  getPageList(@Param(value = "page") PageBean page);


	/**
	 * @Description:查询数量
	 * @return int
	 */
	 public int  getPageCount(@Param(value = "page") PageBean page);


}