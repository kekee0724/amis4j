package com.tianhua.amis4j.infrast.mapper;

import java.util.List;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebApiDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @Description:API数据表mapperDAO接口
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Mapper
public interface WebApiMapper{

	/**
	 * 
	 * @Description:新增或修改
	 * @param webApiDO
	 * @return int
	 */
	public long insert(WebApiDO webApiDO);

	/**
	 * @Description: 通过id删除数据
	 * @param id
	 * @return int
	 */
	public int deleteById(Long id);

	/**
	 * @Description: 通过id查询
	 * @param id
	 * @return ResultDataDto<WebApiDO>
	 */
	public WebApiDO getById(Long id);

	/**
	 * @Description:查询所有数据
	 * @return List<WebApiDO
	 */
	public List<WebApiDO>  getAll();


	/**
	 * @Description:查询所有数据
	 * @return List<WebApiDO
	 */
	public List<WebApiDO>  getByModuleCode(String moduleCode);


	/**
	*
	* @Description:新增或修改
	* @param webApiDO
	* @return int
	*/
	public int update(WebApiDO webApiDO);

	/**
	 * @Description:查询所有数据
	 * @return List<WebApiDO
	 */
	 public List<WebApiDO>  getPageList(@Param(value = "page") PageBean page);


	/**
	 * @Description:查询数量
	 * @return int
	 */
	 public int  getPageCount(@Param(value = "page") PageBean page);


}